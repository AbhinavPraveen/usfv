## Virtualisation Scripts for reasonably securely creating virtual machines in one press.

Does not require root but requires access to `/dev/kvm` if you wish to use KVM (recommended). Being part of the kvm group is one way to do this. Scripts do not (yet) include capability to run graphical applications though they can easily be extended to do so. Note that there must be a space between flags and their values. E.g `-i 5` will work but `-i5` will not.

# virt.conf

Configures all other scripts.

# bootstrap.sh

Will create and store VM images. Creates a serial-booting Glibc Void Linux image using an upstream Alpine image.

# evmx.sh

Creates an 'ephemeral' VM. A VM will be created from a copy of the bootstrapped image. The copy will be deleted when the VM exits.

- `-i` Specifies the ID of the VM. E.g. `-i 5`
- `-f` A qemu hostfwd string. E.g. `-f "tcp:127.0.0.1:5001-:22"`
- `-t` The number of threads to provide the VM. E.g. `-t 3`
- `-m` The memory to provide the VM. E.g. `-m 3G`

# svmx.sh

Creates a 'stateful' VM. A VM will be created from a copy of the bootstraped image. The copy will persist and can be selected using the -i flag.

- `-i` Specifies the ID of the VM. E.g. `-i 5`
- `-f` A qemu hostfwd string. E.g. `-f "tcp:127.0.0.1:5001-:22"`
- `-t` The number of threads to provide the VM. E.g. `-t 3`
- `-m` The memory to provide the VM. E.g. `-m 3G`
- `-d` Destroy the image of the stateful VM either with the default ID, '01', or the ID specified with `-i` 

# uvmx.sh

A stateful VM with the ID 'user'. By default, enables ssh from the host on the specified port.

# uvmssh.sh

Runs its arguments inside the user VM as the non-root user. The user VM must already be started.
