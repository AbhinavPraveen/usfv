#!/bin/sh
SCRIPTDIR="$(dirname $0)"
. "$SCRIPTDIR/virt.conf"

destroy () {
	LOCAL_VERSION="$(cat "$WORKDIR/stateful/$ID/version")"

	rm "$WORKDIR/stateful/$ID/serial-$LOCAL_VERSION.qcow2" && \
	rm "$WORKDIR/stateful/$ID/version" && \
	rmdir "$WORKDIR/stateful/$ID" && \
	exit 0
	
	echo "Failure while deleting old files."
	exit 4
}
while [ $# -ne 0 ]
do
	case $1 in
		-i) ID=$2; shift ;;
		-f) HOSTFWD=",hostfwd=$2"; shift ;;
		-t) THREADS="$2"; shift ;;
		-m) MEMORY="$2"; shift ;;
		-d) DESTROY="1";;
		*)  echo "$1 is not recognized; exiting."; exit 1;;
	esac
	shift
done

test -n "$ID" || ID="01"
test -n "$DESTROY" && destroy
test -n "$MEMORY" || MEMORY="2G"
test -n "$THREADS" || THREADS="2"
mkdir -p "$WORKDIR/stateful/$ID"

LATEST_VERSION="$(cat $WORKDIR/images/voidlinux.version)"
test -z "$LATEST_VERSION" && { echo "A version string could not be found at $WORKDIR/images/voidlinux.version; exiting."; exit 3; }
if test -r "$WORKDIR/stateful/$ID/version"; then
	LOCAL_VERSION="$(cat "$WORKDIR/stateful/$ID/version")"
else
	test -e "$WORKDIR/stateful/$ID/$LATEST_VERSION" && { echo "$LATEST_VERSION was found in $WORKDIR/stateful/$ID but no version string was found. Refusing to continue."; exit 2; }
	cp "$WORKDIR/images/serial-$LATEST_VERSION.qcow2.bak" "$WORKDIR/stateful/$ID/serial-$LATEST_VERSION.qcow2"
	LOCAL_VERSION="$LATEST_VERSION"
	echo "$LOCAL_VERSION" > "$WORKDIR/stateful/$ID/version"
fi
$QEMU -drive file="$WORKDIR/stateful/$ID/serial-$LOCAL_VERSION.qcow2",if=virtio,media=disk,format=qcow2 -m $MEMORY -smp $THREADS -enable-kvm -device virtio-net,netdev=n0 -netdev user,id=n0$HOSTFWD -nographic
