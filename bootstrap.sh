#!/bin/sh
SCRIPTDIR="$(dirname $0)"
. "$SCRIPTDIR/virt.conf"

get_version() {
	$HTTP_FETCH "$1" \
	| $EGREP "$2" \
	| tail -n 1 \
	|| { echo "Could not get latest version info; exiting."; exit 1; }
}
http_get() {
	test -e "$WORKDIR/images/$1" \
		|| $HTTP_FETCH "$2/$1" > "$WORKDIR/images/$1" \
		|| { echo "$1 did not exist at $WORKDIR/images/$1 and we tried to fetch it but failed; exiting."; exit 4; }
}
fetch_image() {
	VERSION="$(get_version "$1" "$2")"
	test -n "$VERSION" || { echo "$3 version string empty after fetch; exiting."; exit 2; }
	mkdir -p "$WORKDIR/images"
	echo "$VERSION" > "$WORKDIR/images/$3.version"
	http_get $VERSION $1
}

echo "
Fetching Alpine image.
"

fetch_image "$ALPINE_MIRROR/latest-stable/releases/x86_64" "alpine-virt-[0-9.]*(_rc[0-9])?-x86_64\.iso" "alpine"
ALPINE_VERSION="$(cat $WORKDIR/images/alpine.version)"

echo "
Image fetched successfully with version $ALPINE_VERSION.
"

echo "
Fetching secondary (VoidLinux Image).
"

fetch_image "$VOID_MIRROR/live/current/" "void-x86_64-ROOTFS-[0-9]{8}\.tar\.xz" "voidlinux"
VOID_VERSION="$(cat $WORKDIR/images/voidlinux.version)"

echo "
Image fetched successfully with version $VOID_VERSION.
"

echo "
Bootstrapping secondary images.
"

mkdir -p "$WORKDIR/etc/ssh/"
ssh-keygen -A -f "$WORKDIR" || { echo "SSH keys could not be generated; exiting."; exit 13; }
echo "[127.0.0.1]:* ssh-ed25519 $(awk '{print $2}' "$WORKDIR/etc/ssh/ssh_host_ed25519_key.pub")" > "$WORKDIR/known_hosts"
test -z "$(cat "$WORKDIR/known_hosts")" && { echo "An ssh known_hosts entry could not be created at $WORKDIR/known_hosts; exiting. Check that awk works."; exit 14; }
test -e "$SSH_KEY" || ssh-keygen -t ed25519 -f "$SSH_KEY" -N "" || { echo "An SSH key could not be found at $SSH_KEY and could not be created."; exit 15; }

test ! -e "$WORKDIR/images/serial-$VOID_VERSION.qcow2" || rm "$WORKDIR/images/serial-$VOID_VERSION.qcow2" && \
	qemu-img create -f qcow2 "$WORKDIR/images/serial-$VOID_VERSION.qcow2" 10G || { echo "Failed to create a fresh qcow2 image at $WORKDIR/images/serial-$VOID_VERSION.qcow2; exiting."; exit 5; }

ESCAPED_WORKDIR="$(printf '%s\n' "$WORKDIR" | sed -e 's/[\/&]/\\&/g')"
ESCAPED_ALPINE_MIRROR="$(printf '%s\n' "$ALPINE_MIRROR" | sed -e 's/[\/&]/\\&/g')"
SSH_HOSTKEY_B64="$(base64 -w 0 "$WORKDIR/etc/ssh/ssh_host_ed25519_key")"
ESCAPED_VOID_QCOW2_DIR="$(printf '%s\n' "$WORKDIR/images/serial-$VOID_VERSION.qcow2" | sed -e 's/[\/&]/\\&/g')"
ESCAPED_SSH_KEY="$(printf '%s\n' "$SSH_KEY" | sed -e 's/[\/&]/\\&/g')"
ESCAPED_QEMU="$(printf '%s\n' "$QEMU" | sed -e 's/[\/&]/\\&/g')"

test -z "$ESCAPED_WORKDIR" && { echo "\$WORKDIR could not be escaped properly. Check that printf and sed work correctly; exiting."; exit 9; }
test -z "$ESCAPED_ALPINE_MIRROR" && { echo "\$ALPINE_MIRROR could not be escaped properly. Check that \$ALPINE_MIRROR is set correctly; exiting."; exit 10; }
test -z "$SSH_HOSTKEY_B64" && { echo "\$SSH_HOSTKEY_B64 could not be set. Check that base64 works properly; exiting."; exit 11; }
test -z "$ESCAPED_VOID_QCOW2_DIR" && { echo "\$ESCAPED_VOID_QCOW2_DIR could not be set; exiting."; exit 12; }
test -z "$ESCAPED_SSH_KEY" && { echo "\$ESCAPED_SSH_KEY could not be set; exiting."; exit 12; }
test -z "$ESCAPED_QEMU" && { echo "\$ESCAPED_QEMU could not be set; exiting."; exit 12; }

sed \
"s/%log_file%/$LOG_FILE/g
s/%iso_path%/$ESCAPED_WORKDIR\/images\/$ALPINE_VERSION/g
s/%alpine_mirror%/$ESCAPED_ALPINE_MIRROR/g
s/%ssh_pubkey%/$(cat "$SSH_KEY.pub")/g
s/%ssh_hostkey%/$SSH_HOSTKEY_B64/g
s/%void_version%/$VOID_VERSION/g
s/%workdir%/$ESCAPED_WORKDIR/g
s/%void_img%/$ESCAPED_VOID_QCOW2_DIR/g
s/%root_password%/$ROOT_PASSWORD/g
s/%user_password%/$USER_PASSWORD/g
s/%user_name%/$USER_NAME/g
s/%ssh_port%/$SSH_BOOTSTRAP_PORT/g
s/%ssh_key%/$ESCAPED_SSH_KEY/g
s/%grub_timeout%/$GRUB_TIMEOUT/g
s/%qemu%/$ESCAPED_QEMU/g" \
"$SCRIPTDIR/expect.template" > "$WORKDIR/expect.sh" || \
{ echo "Could not create an expect script from expect.template; exiting."; exit 6; }
expect -f "$WORKDIR/expect.sh" > "$WORKDIR/expect_latest.log" || { echo "The produced expect script did not execute succesfully; exiting."; exit 7; }
cp "$WORKDIR/images/serial-$VOID_VERSION.qcow2" "$WORKDIR/images/serial-$VOID_VERSION.qcow2.bak" || { echo "An image $WORKDIR/images/serial-$VOID_VERSION.qcow2 was created succesfully but a backup could not be created at $WORKDIR/images/serial-$VOID_VERSION.qcow2.bak; exiting."; exit 8; }

echo "
A Secondary Void Linux image was produced successfully at $WORKDIR/images/serial-$VOID_VERSION.qcow2.bak!
"

mkdir -p "$WORKDIR/ephemeral"
mkdir -p "$WORKDIR/stateful"

exit 0
