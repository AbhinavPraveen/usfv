#!/bin/sh
SCRIPTDIR="$(dirname $0)"
. "$SCRIPTDIR/virt.conf"

while [ $# -ne 0 ]
do
	case $1 in
		-i) ID=$2; shift ;;
		-f) HOSTFWD=",hostfwd=$2"; shift ;;
		-t) THREADS="$2"; shift ;;
		-m) MEMORY="$2"; shift ;;
		*)  echo "$1 is not recognized; exiting."; exit 1;;
	esac
	shift
done

createID () {
	ID="$(cat /dev/urandom | tr -dc A-Z0-9 | head -c 2)"
	if mkdir "$WORKDIR/ephemeral/$ID";then
		echo $ID && return 0
	else
		createID
	fi
}

test -n "$ID" && mkdir "$WORKDIR/ephemeral/$ID" || ID="$(createID)"
test -n "$MEMORY" || MEMORY="2G"
test -n "$THREADS" || THREADS="2"

VERSION="$(cat $WORKDIR/images/voidlinux.version)"
cp "$WORKDIR/images/serial-$VERSION.qcow2.bak" "$WORKDIR/ephemeral/$ID/serial-$VERSION.qcow2"
$QEMU -drive file="$WORKDIR/ephemeral/$ID/serial-$VERSION.qcow2",if=virtio,media=disk,format=qcow2 -m $MEMORY -smp $THREADS -enable-kvm -device virtio-net,netdev=n0 -netdev user,id=n0$hostfwd -nographic
rm "$WORKDIR/ephemeral/$ID/serial-$VERSION.qcow2" && rmdir "$WORKDIR/ephemeral/$ID" || { echo "The ephemeral directory $WORKDIR/ephemeral/$ID coudld not be deleted after usage. This is safe to delete unless it has been modified externally."; exit 1; }
