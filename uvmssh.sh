#!/bin/sh
SCRIPTDIR="$(dirname $0)"
. "$SCRIPTDIR/virt.conf"

ssh "$USER_NAME@127.0.0.1" -p "$SSH_PORT" -o "UserKnownHostsFile=$WORKDIR/known_hosts" -i "$SSH_KEY" "$@"
